package assignment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alireza
 */
@Repository
public interface WebsiteRepository extends CrudRepository<Website, Integer> {

    @Query("select w from Website w WHERE w.websiteId.wsDate = ?1 ORDER BY w.wsVisits desc")
    public Iterable<Website> findByDate(Long date);

    public Page<Website> findByDate(Long date, Pageable pageable);

}
