/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 *
 * @author alireza
 */
@Embeddable
public class WebsiteId implements Serializable {

    @Size(max = 256)
    @Column(name = "ws_name")
    private String wsName;
    @Column(name = "ws_date")
    private Long wsDate;

    public String getWsName() {
        return wsName;
    }

    public void setWsName(String wsname) {
        this.wsName = wsname;
    }

    public Long getWsDate() {
        return wsDate;
    }

    public void setWsDate(Long wsdate) {
        this.wsDate = wsdate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.wsName);
        hash = 79 * hash + Objects.hashCode(this.wsDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WebsiteId other = (WebsiteId) obj;
        if (!Objects.equals(this.wsName, other.wsName)) {
            return false;
        }
        if (!Objects.equals(this.wsDate, other.wsDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WebsiteId{" + "wsName=" + wsName + ", wsDate=" + wsDate + '}';
    }
}
