package assignment;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@SpringBootApplication
@RestController
public class SampleController {

    private static final String PATTERN = "yyyy-MM-dd";

    private static final ThreadLocal<SimpleDateFormat> TL_SDF = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(PATTERN);
        }

    };

    private static Date parse(String source) throws Exception {
        return TL_SDF.get().parse(source);
    }

    private static String format(Date source) {
        return TL_SDF.get().format(source);
    }

    @Autowired(required = true)
    @Qualifier("websiteRepository")
    private WebsiteRepository repository;

    @RequestMapping("/json")
    @ResponseBody
    public Map json() {
        Map<String, Object> map = new HashMap<>();
        map.put("date", new java.util.Date());
        map.put("name", "Alireza");

        List<Object> list = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            list.add(Math.pow((double) 2, (double) i));

        }

        map.put("list", list);
        return map;
    }

    @RequestMapping("/list")
    public Map listAllWebsites(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        System.out.println("listAllWebsites : date = " + date);

        List<Object> list = new LinkedList<>();

        Iterable<Website> itw;
        if (date != null) {
            int i = 0;
            Iterator<Website> iterator = repository.findByDate(date.getTime()).iterator();
            while (iterator.hasNext()) {
                Website website = iterator.next();
                System.out.println(website);
                Map<String, Object> row = new HashMap<>();
                row.put("name", website.getWebsiteId().getWsName());
                row.put("date", format(new Date(website.getWebsiteId().getWsDate())));
                row.put("visits", website.getWsVisits());
                list.add(row);
                i++;
                if (i == 5) {
                    break;
                }
            }
        } else {
            itw = repository.findAll();
            for (Website website : itw) {
                Map<String, Object> row = new HashMap<>();
                row.put("name", website.getWebsiteId().getWsName());
                row.put("date", format(new Date(website.getWebsiteId().getWsDate())));
                row.put("visits", website.getWsVisits());
                list.add(row);
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("records", list);
        return map;
    }

//    @RequestMapping(value = "/upload.html", method = RequestMethod.GET)
//    public String bypass() {
//        return "/upload.html";
//    }
    @RequestMapping(value = "/uploadcsv", method = RequestMethod.POST)
    public ResponseEntity uploadFile(MultipartHttpServletRequest request) {
        List<Object[]> body = new LinkedList<>();

        try {
            char delim = '|';

            String strDelim = request.getParameter("delim");
            if (strDelim != null && strDelim.isEmpty() == false) {
                delim = strDelim.charAt(0);
            }

            List<Website> batch = new LinkedList<>();

            java.util.Iterator<String> it = request.getFileNames();
            while (it.hasNext()) {
                String name = it.next();
                MultipartFile file = request.getFile(name);
                processInputStream(file.getInputStream(), delim, batch, body);
            }

            repository.save(batch);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity(body, HttpStatus.OK);
    }

    private static void processInputStream(InputStream in, char delim, List batch, List body) {
        String line;
        try (Scanner reader = new Scanner(in)) {

            // skip header line
            if (reader.hasNext()) {
                line = reader.nextLine();
            }
            while (reader.hasNext()) {
                line = reader.nextLine();
                try {
                    Website website = parseLine(line, delim);
                    batch.add(website);

                    body.add(new String[]{line, "ok"});

                } catch (Exception e) {
                    System.err.println("line=" + line);
                    e.printStackTrace();

                    body.add(new String[]{line, e.toString()});
                }
            }
        }
    }

    private static Website parseLine(String line, char delim) throws Exception {
        Website website = new Website();
        website.setWebsiteId(new WebsiteId());

        List<String> tokens = fastSplit(line, delim);

        if (0 < tokens.size()) {
            Date wsDate = parse(tokens.get(0));
            website.getWebsiteId().setWsDate(wsDate.getTime());
        }

        if (1 < tokens.size()) {
            String wsName = tokens.get(1);
            website.getWebsiteId().setWsName(wsName);
        }

        if (2 < tokens.size()) {
            String token = tokens.get(2);
            Long wsVisits = Long.parseLong(token);
            website.setWsVisits(wsVisits);
        }

        return website;
    }

    private static List<String> fastSplit(String line, char delim) {
        List<String> tokens = new LinkedList<>();

        StringBuilder buf = new StringBuilder();
        char[] array = line.toCharArray();
        for (int i = 0; i < array.length; i++) {
            char c = array[i];

            if (c == delim) {
                String token = buf.toString();
                tokens.add(token);
                buf = new StringBuilder();
            } else if (c == '\"' || c == '\'') {
                // ignore quote symbols
            } else {
                buf.append(c);
            }
        }

        if (buf.length() > 0) {
            String token = buf.toString();
            tokens.add(token);
        }

        return tokens;
    }

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext run = SpringApplication.run(SampleController.class, "--debug");

        for (String name : run.getBeanDefinitionNames()) {
            System.out.println("'" + name + "' : " + run.getBean(name));
        }

        String name = "websiteRepository";
        WebsiteRepository repository = (WebsiteRepository) run.getBean(name);

        for (Website website : repository.findAll()) {
            System.out.println("website : " + website);
        }

    }

}
