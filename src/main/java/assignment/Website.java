/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author alireza
 */
@Entity
@Table(name = "website")
@NamedQueries({
    @NamedQuery(name = "Website.findAll", query = "SELECT w FROM Website w"),
    @NamedQuery(name = "Website.findByDate", query = "SELECT w FROM Website w where w.websiteId.wsDate = ?1")
}
)
public class Website implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private WebsiteId websiteId;
    @Column(name = "ws_visits")
    private Long wsVisits;

    public Website() {
    }

    public WebsiteId getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(WebsiteId websiteId) {
        this.websiteId = websiteId;
    }

    public Long getWsVisits() {
        return wsVisits;
    }

    public void setWsVisits(Long wsVisits) {
        this.wsVisits = wsVisits;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.websiteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Website other = (Website) obj;
        if (!Objects.equals(this.websiteId, other.websiteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Website{" + "websiteId=" + websiteId + '}';
    }
}
